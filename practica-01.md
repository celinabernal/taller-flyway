# Práctica \#1

1. Comprueba que tengas acceso a un servidor de bases de datos relacional, de preferencia mediante nombre de usuario y contraseña.

 - Puedes utilizar la línea de comandos o un cliente de correo como Dbeaver.
 - Dbeaver: https://dbeaver.io/
 - Dataedo: https://dataedo.com/

2. Comprueba que tienes los privilegios para crear bases de datos, tablas y demás objetos con tu cuenta de acceso.
