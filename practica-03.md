# Práctica 3
## Descarga del software FlywayDB

1. Ingresa al sitio https://flywaydb.org/download y descarga la versión "Community Edition" de Flyway para línea de commando (es la que trae la etiqueta "commandline" en el nombre de archivo)

- Asegurate de descargar la versión que corresponda a tu sistema operativo.

2. Al momento de escribir esto, la última versión de Flyway es la 7.7. Si necesitas descargar versiones anteriores o tienes curiosidad sobre las versiones publicadas de la herramienta, puedes ingresar al siguiente link: https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/

3. Una vez descargado el software, desempaquetalo, si es necesario, y guardalo en el directorio:

```
- taller-flyway
 - software
```

Por ejemplo, la versión descargada quedaría así:

```
- taller-flyway
 - software
   - flyway-7.7.0
```

## Estructura de directorios y archivos de FlywayDB

1. El archivo **ejecutable** se encuentra en la raíz del directorio desempaquetado. Por ejemplo, la versión de Windows trae este ejecutable:

```
- flyway-7.7.0
  - flyway
  - flyway.cmd
```
2. El archivo de **configuración** se encuentra en el directorio **conf** Este es el archivo que sirve para especificar la conexión a nuestra base de datos.

```
- flyway-7.7.0
  - conf
    - flyway.conf
```

3. Las **migraciones** (los scripts SQL) se colocan en el directorio **sql**

```
- flyway-7.7.0
  - sql
```

4. La herramienta utiliza un entorno de ejecución de Java para funcionar así como drivers JDBC para establecer la conexión con la base de datos. En el directorio **jre** se encuentra el ambiente de ejecución de FlywayDB y en el directorio **drivers** están los **.jar** de los controladores para los manejadores de bases de datos que soporta FlywayDB.

```
- flyway-7.7.0
  - drivers
  - jre
```
